Instructions to set up a kubernetes cluster on your machine 

Prerequisite
1. Ansible is configured on your host machine.(Follow install-ansible file in folder to install and configure ansible) 
2. Create a block of hosts by name of [remotemachine] in /etc/ansible/hosts and add all hosts where you want to set this cluster. 

If docker was previously installed please remove that by typing 

"ansible-playbook remove-docker.yml -K" in your terminal...

If kubernetes was previously installed please remove that by typing 

"ansible-playbook remove-kubernetes.yml -K" in your terminal...

to set docker please type command 

"ansible-playbook install-docker.yml -K" in your terminal...

to set kubernetes-cluster please type command 

"ansible-playbook install-kubernetes.yml -K" in your terminal...


if there was no errors your kubernetes cluster is up and running. 

Now go to your host machine where you just set this node and start using it.. 

Thank You. 
